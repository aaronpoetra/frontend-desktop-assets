/* End Sticky PDP Submenu */
$(window).load(function () {
	/* Sticky Top 
	$("#top-sticky").sticky({
		topSpacing: 0
	});
	*/
	/* Sticky PDP Subnavigation 
	$("#sticky-subnavigation").sticky({
		topSpacing: 70
	});
	*/
	/* Main Slider */
	$('.slider-images').owlCarousel({
		loop: false,
		nav: true,
		navText: ['<i class="anticon icon-left"></i>', '<i class="anticon icon-right"></i>'],
		items: 1,
		dots: true,
		margin: 0,
		autoplay: true,
		autoplayTimeout: 5000,
		autoplayHoverPause: false
	});
	/*Main Slider 2*/
	$('.slider-images-2').owlCarousel({
		loop: false,
		nav: true,
		navText: ['<i class="anticon icon-left"></i>', '<i class="anticon icon-right"></i>'],
		items: 1,
		dots: true,
		margin: 0,
		autoplay: true,
		autoplayTimeout: 5000,
		autoplayHoverPause: false
	});
	/* Owl Brand*/
	$('.owl-brand').owlCarousel({
		loop: false,
		nav: true,
		items: 1,
		dots: false,
		navText: ['<i class="anticon icon-left"></i>', '<i class="anticon icon-right"></i>']
	});
	/* Owl Main Slider 2*/
	$('.mainBanner-2').owlCarousel({
		loop: false,
		nav: true,
		items: 1,
		dots: true,
		lazyLoad: true,
		navText: ['<i class="anticon icon-left"></i>', '<i class="anticon icon-right"></i>']
	});
});
/*show transaksi detail*/
// function showtracking() {
// 	$("#tracking-detail").show();
// 	$("#tracking-search").show();
// }
$(document).ready(function () {
	$("#tracking-search").show();
	/*Subcategory mega-menu*/
	$(".mega-subcategory ul>li").on('click', function () {
		var catval = $(this).attr("id");
		$("#cat-val").val(catval);
	});
	$(".mega-category>li>a").click(function () {
		$(this).closest("li").find(".mega-subcategory").show();
	});
	/*----------------------------------------------Function Overlay------------------------------------*/
	/*show category mega menu*/
	// $("body,a").click(function (e) {
	// 	if (!$(e.target).is('.mega-menu, .mega-menu *')) {
	// 		$(".mega-menu").hide();
	// 	}
	// });
	/* Search Click */
	// $('.js-show-panel').click(function (e) {
	// 	$(this).next('.js-panel-tip').fadeIn('medium');
	// 	$(this).closest(".search-bar").find('.overlay-dark').toggle();
	// 	$(".dropoverlay").find(".overlay-dark").hide();
	// 	$(document).bind('focusin.js-panel-tip, click.js-panel-tip', function (e) {
	// 		if ($(e.target).closest('.js-panel-tip, .js-show-panel').length) return;
	// 		$(document).unbind('.js-panel-tip');
	// 		$('.js-panel-tip').fadeOut('medium');
	// 		$(".search-bar").find('.overlay-dark').hide();
	// 	});
	// 	/*Search Typing*/
	// 	$('.js-show-panel').on('input', function (e) {
	// 		if ($(this).val().length === 0) {
	// 			$('.js-panel-tip').fadeIn('medium');
	// 			$('.js-panel-type').fadeOut('medium');
	// 		} else {
	// 			$('.js-panel-tip').hide();
	// 			$('.js-panel-type').fadeIn('medium');
	// 			$("#val-search").text($(this).val());
	// 			$(document).bind('focusin.js-panel-type, click.js-panel-type', function (e) {
	// 				if ($(e.target).closest('.js-panel-type, .js-show-panel').length) return;
	// 				$(document).unbind('.js-panel-type');
	// 				$('.js-panel-type').fadeOut('medium');
	// 				$(".search-bar").find('.overlay-dark').hide();
	// 			});
	// 		}
	// 	});
	// });
	/*show cart*/
	// $(".dropoverlay .showdrop").click(function (e) {
	// 	$('.dropoverlay .showdrop').not(this).closest(".dropoverlay").find(".dropdown-top").hide();
	// 	$('.dropoverlay .showdrop').not(this).removeClass("active");
	// 	$('.dropoverlay .showdrop').not(this).closest(".dropoverlay").find(".overlay-dark").hide();
	// 	$(".search-container").find(".overlay-dark").hide();
	// 	$(this).closest(".dropoverlay").find(".dropdown-top").toggle();
	// 	$(this).closest(".dropoverlay").find(".overlay-dark").toggle();
	// 	$(this).toggleClass("active");
	// 	e.stopPropagation();
	// });
	// $(document).on("click", function (e) {
	// 	if (!$(e.target).is('.dropdown-top, .dropdown-top *,.search-container,.search-container *,.js-show-panel,.mega-menu, .mega-menu *')) {
	// 		$(".dropdown-top").hide();
	// 		$(".overlay-dark").hide();
	// 		$(".showdrop").removeClass("active");
	// 	}
	// });
	// $(".closedrop").on("click", function (e) {
	// 	$(this).closest(".dropoverlay").find(".dropdown-top").hide();
	// 	$(this).closest(".dropoverlay").find(".overlay-dark").hide();
	// 	$(this).closest(".dropoverlay").find(".showdrop").removeClass("active");
	// });
	/*Show Password*/
	$(".showpassword").click(function () {
		if ($(this).closest(".form-icon").find(".password").attr("type") == "password") {
			$(this).closest(".form-icon").find(".password").attr("type", "text");
			$(this).find("i").removeClass("icon-eyeo");
			$(this).find("i").addClass("icon-eye");
		} else {
			$(this).closest(".form-icon").find(".password").attr("type", "password");
			$(this).find("i").removeClass("icon-eye");
			$(this).find("i").addClass("icon-eyeo");
		}
	});
	$('.product-container-promo').hover(function () {
		$(this).addClass('product-container-promo-hovered')
	}, function () {
		$(this).removeClass('product-container-promo-hovered')
	});
	// /* Product Variant - noted this one */
	// $(".product-container .color-container").click(function () {
	// 	var attrcolor = $(this).attr("attr-color");
	// 	$(this).closest(".product-container .bitmap").find(".color-container").removeClass("selected");
	// 	$(this).addClass("selected");
	// 	$(this).closest(".product-container .bitmap").find(".item").removeClass("selected");
	// 	$(this).closest(".product-container .bitmap").find("#" + attrcolor).addClass("selected");
	// });
	/* Color Filter */
	$("#color-filter-choose>li").click(function () {
		$("#color-filter-choose>li").removeClass("active");
		var datacolor = $(this).attr("data-color");
		$(this).addClass("active");
		$("#color-filter").val(datacolor);
	});
	/* Fancybox */
	$('.fancybox').fancybox();

	/* banner side */
	$('.bannerSlider').owlCarousel({
		loop: false,
		nav: true,
		navText: ['<i class="anticon icon-left"></i>', '<i class="anticon icon-right"></i>'],
		autoplay: true,
		items: 1,
		dots: true
	});
	/* review side */
	$('.reviewSlider').owlCarousel({
		loop: false,
		nav: true,
		navText: ['<i class="anticon icon-left"></i>', '<i class="anticon icon-right"></i>'],
		autoplay: true,
		items: 1,
		dots: true
	});
	/* Color Variant product */
	$("#cat_color li").click(function () {
		$("#cat_color li").removeClass("active");
		var dataval = $(this).find("a").attr("data-attr");
		$("#" + dataval).addClass("active");
		$("#color_choose").val(dataval);
	});
	/*sticky pdp normal*/

	$('.sub-navigation-sticky a').on('click', function (e) {
		e.preventDefault();
		$(document).off("scroll");
		$('a').each(function () {
			$(this).removeClass('active');
		})
		$(this).addClass('active');
		var target = this.hash;
		$target = $(target);
		$('html, body').stop().animate({
			'scrollTop': $target.offset().top + 100
		}, 600, 'swing', function () {
			window.location.hash = target;
			$(document).on("scroll", onScroll);
		});
	});
	/* Countdown */
	$('#flash-countdown').countdown($("#flash-countdown").attr("attr-date"), function (event) {
		var $this = $(this).html(event.strftime('' + '<span>%H</span>' + '<span>%M</span>' + '<span>%S</span>'));
	}); 
});

function onScroll(event) {
	var scrollPosition = $(document).scrollTop();
	$('.sub-navigation-sticky a').each(function () {
		var currentLink = $(this);
		var refElement = $(currentLink.attr("href"));
		if (refElement.position().top <= scrollPosition && refElement.position().top + refElement.height() > scrollPosition) {
			$('.sub-navigation-sticky a').removeClass("active");
			currentLink.addClass("active");
		} else {
			currentLink.removeClass("active");
		}
	});
}

/* Quantity */
$(document).ready(function () {
	var n = 1;
	$('.qty-plus').on('click', function () {
		$(this).closest(".qty-input").find(".qty-text").val(++n);
	})
	$('.qty-min').on('click', function () {
		if (n >= 1) {
			$(this).closest(".qty-input").find(".qty-text").val(--n);
		} else {}
	});
});
/* Bg Flash */
$(document).ready(function () {
	$(".background-container").css('background', function () {
		return $(this).data('bg')
	});
});

/*copy clipboard*/
function copyToClipboard(element) {
	var $temp = $("<input>");
	$("body").append($temp);
	$temp.val($(element).text()).select();
	document.execCommand("copy");
	$temp.remove();
}
$(function () {
	$("#BrandShowMore").on("click", function () {
		$("#brand-content-arrow").toggleClass("open");
		$(this).toggleClass("active");
	});
});

/*sticky pdp normal submenu*/
$(document).ready(function () {
  $(document).on("scroll", onScroll);
  $('.sub-navigation-sticky a').on('click', function (e) {
    e.preventDefault();
    $(document).off("scroll");
    $('a').each(function () {
      $(this).removeClass('active');
    })
    $(this).addClass('active');
    var target = this.hash;
    $target = $(target);
    $('html, body').stop().animate({
      'scrollTop': $target.offset().top - 135
    }, 600, 'swing', function () {});
  });
});